﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Svg;

namespace DoodleBot
{
    class SVGProcessor
    {
        public SVGProcessor(){
            elementsList = new List<Path>();
            pathItem = new Path();
            docNew = new SvgDocument();
            ShrinkRate = 1;
        }

        public bool IsThereMoreShapes { get; private set; }
        public SvgDocument OriginalFile { get; private set; }
        public SvgDocument FilteredFile { get; private set; }
        public float ShrinkRate { get; private set; }

        private List<Path> elementsList;
        private Path pathItem;
        private SvgDocument docNew;


        private SvgRectangle rectangleType = new SvgRectangle();
        private SvgPath pathType = new SvgPath();

        internal void LoadOriginalFile(string fileName) {

            try
            {
                SvgDocument doc = SvgDocument.Open(fileName);

                if (doc.Height > ControllerClass.DISPLAY_HEGHT
                        || doc.Width > ControllerClass.DISPLAY_WIDTH)
                {
                    SetShrinkRate(doc.Width, doc.Height);
                    //doc.Width = ShrinkRate * doc.Width;
                    //doc.Height = ShrinkRate * doc.Height;
                }
                Console.WriteLine(doc.Width + "\n" + doc.Height);

                OriginalFile = doc;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void SetShrinkRate(SvgUnit width, SvgUnit height)
        {
            float resultRate;

            if (width - ControllerClass.DISPLAY_WIDTH > height - ControllerClass.DISPLAY_HEGHT) /* Need to shrink the width */
            {
                resultRate = ControllerClass.DISPLAY_WIDTH / width;
            }
            else
            {
                resultRate = ControllerClass.DISPLAY_HEGHT / height;
            }

            ShrinkRate = resultRate;
        }

        internal Image FileToBitmap(SvgDocument file)
        {
            try
            {
                SvgDocument doc = file;
                Image retIm = new Bitmap(doc.Draw(), new Size((int)(OriginalFile.Width * ShrinkRate), (int)(OriginalFile.Height * ShrinkRate)));
                
                return retIm;
            }
            catch (Exception)
            {

                throw;//needs to be handled
            }
        }

        internal void Filter(string currentFileName)
        {
            //SvgDocument doc = SvgDocument.Open(currentFileName);

            SvgDocument docResult = Recourse(OriginalFile.Children);
            FilteredFile = docResult;
            //this will erase all unnecessary line from original svg and maybe creates a filtered file on hard disk.
            //returns the created filtered file path+name
        }

        private SvgDocument Recourse(SvgElementCollection elements)
        {
            docNew = new SvgDocument();
            foreach (SvgElement elem in elements)
            {
                if (elem.HasChildren() == true)
                {
                    Recourse(elem.Children);
                }
                else
                {
                    //in case of rectangle
                    if (elem.GetType() == rectangleType.GetType())
                    {
                        docNew.Children.Add(elem);
                    }

                    //in case of path
                    if (elem.GetType() == pathType.GetType())
                    {
                        docNew.Children.Add(elem);
                    }
                }
            }
            return docNew;
        }

        internal void ReInit()
        {
            elementsList = new List<Path>();
            pathItem = new Path();
            docNew = new SvgDocument();
        }

        internal Image GetFilteredImage()
        {

            Image result = new Bitmap((int)ControllerClass.DISPLAY_WIDTH, (int)ControllerClass.DISPLAY_HEGHT);
            Graphics g = Graphics.FromImage(result);
            Point prevpoint = new Point(0,0);
            g.Clear(Color.White);
            foreach (Path path in elementsList)
            {
                for (int i = 0; i < path.GetPoints().Count; i++)
                {
                    if (i == 0)
                    {
                        prevpoint = path.GetPoints().ElementAt(i);
                    }
                    else
                    {
                        g.DrawLine(Pens.Black, prevpoint.X, prevpoint.Y, path.GetPoints().ElementAt(i).X, path.GetPoints().ElementAt(i).Y);
                        prevpoint = path.GetPoints().ElementAt(i);
                    }
                } 
            }

            return result;
        }

        internal void ProcessSvg(SvgDocument file)
        {
            SvgDocument doc = file;
            Console.WriteLine(doc.Children);

            Shape shape;

            SvgElementCollection elements = doc.Children;
            foreach (SvgElement elem in elements)
            {
                shape = ShapeCreator.GetShape(elem);
                elementsList.Add(shape.ToPath());
                Console.WriteLine(elem.ToString());
            }
            //RecoursiveProcessing(elements);
        }

        /*private void RecoursiveProcessing(SvgElementCollection elements)
        {
            foreach (SvgElement elem in elements)
            {
                if (elem.HasChildren() == true)
                {
                    RecoursiveProcessing(elem.Children);
                }
                else
                {
                    pathItem = new Path();

                    //in case of rectangle
                    if (elem.GetType() == rectangleType.GetType())
                    {

                        uint x = (uint)((SvgRectangle)elem).X;
                        uint y = (uint)((SvgRectangle)elem).Y;
                        uint width = (uint)((SvgRectangle)elem).Width;
                        uint height = (uint)((SvgRectangle)elem).Height;

                        //first corner
                        pathItem.AddPoint(new Point(x, y));

                        //second corner
                        pathItem.AddPoint(new Point(x, y + height));

                        //third corner
                        pathItem.AddPoint(new Point(x + width, y + height));

                        //fourth corner
                        pathItem.AddPoint(new Point(x + width, y));

                        //first corner for finish
                        pathItem.AddPoint(new Point(x, y));
                    }

                    //in case of path
                    if (elem.GetType() == pathType.GetType())
                    {
                        int i = 0;
                        foreach (var item in ((SvgPath)elem).PathData)
                        {
                            i++;
                            if (i != ((SvgPath)elem).PathData.Count)
                            {
                                pathItem.AddPoint(new Point((uint)item.End.X, (uint)item.End.Y));
                            }

                        }

                    }

                    elementsList.Add(pathItem);
                }
            }
        }*/

        internal List<Path> getList()
        {
            return elementsList;
        }
    }
}
