﻿using System;
using System.IO.Ports;
using System.Threading;

namespace DoodleBot
{
    internal class Communication
    {
        private SerialPort sPort;
        private const int ADDCMD_LENGTH = 9;
        private const Byte CMDTYPE_MOVE = 1;
        private const Byte CMDTYPE_LINE = 2;
        private const Byte CMDTYPE_PEN = 0;

        private const int RUN_LENGTH = 4;

        private const int MOVE_LENGTH = 8;
        private const int CALIB_LENGTH = 4;
        private const int PEN_LENGTH = 5;
        public const int PEN_UP = 0;
        public const int PEN_DOWN = 1;

        private const Byte comHeader = 0xAA;
        private const Byte comCRC = 0x00;

        public Communication()
        {

        }

        internal void OpenPort(string name)
        {
            sPort = new SerialPort(name, 115200);
            if (sPort.IsOpen == false)
            {
                sPort.Open();

            }
        }

        internal void AddToQueue(object v)
        {
            //add new value to buffer and send it out
        }

        internal void send()
        {
            //comDataLength = 0x04;
            //comCommand = 0x03;

            //messageLength = comDataLength + 0x03;

            //message = new Byte[messageLength];

            //message[0] = comHeader;
            //message[1] = comDataLength;
            //message[2] = comCommand;
            //message[3] = 0x09;
            //message[4] = 0x08;
            //message[5] = 0x04;
            //message[6] = comCRC;

            //aa 04 03 01 02 03 00

            Console.WriteLine("hello");
            //sPort.Open();
            //sPort.Write(move(1000,1000), 0, MOVE_LENGTH);

            sPort.Write(pen(PEN_DOWN), 0, PEN_LENGTH);
            Thread.Sleep(1000);
            sPort.Write(pen(PEN_UP), 0, PEN_LENGTH);

            //sPort.Close();

        }

        internal Byte[] calibrate()
        {
            Byte[] message = new Byte[CALIB_LENGTH];
            message[0] = comHeader;
            message[1] = 0x01;
            message[2] = 0x01;
            message[3] = comCRC;

            return message;
            //low = (x >> 0) & 0xff;
            //hi = (x >> 8) & 0xFF; 
        }

        internal void ClosePort()
        {
            if (sPort != null && sPort.IsOpen == true)
            {
                sPort.Close(); 
            }
        }

        internal Byte[] move(uint x, uint y)
        {
            Byte[] message = new Byte[MOVE_LENGTH];
            message[0] = comHeader;
            message[1] = 0x05;
            message[2] = 0x02;
            message[3] = (Byte)((x >> 0) & 0xff);
            message[4] = (Byte)((x >> 8) & 0xff);
            message[5] = (Byte)((y >> 0) & 0xff);
            message[6] = (Byte)((y >> 8) & 0xff);
            message[7] = comCRC;

            return message;
        }

        internal Byte[] pen(uint state)
        {
            Byte[] message = new Byte[PEN_LENGTH];
            message[0] = comHeader;
            message[1] = 0x02;
            message[2] = 0x03;
            message[3] = (Byte)(state);
            message[4] = comCRC;

            return message;
        }

        internal void SendCalib()
        {
            try
            {
                sPort.Write(calibrate(), 0, CALIB_LENGTH);
                Thread.Sleep(100);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void SendMove(uint x, uint y)
        {
            try
            {
                if (x <= 5000 && x >= 0
                        && y <= 7000 && y >= 0)
                {
                    sPort.Write(addCmd(CMDTYPE_MOVE, (uint)((2 * x) / 3), y), 0, ADDCMD_LENGTH);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void SendLine(uint x, uint y)
        {
            try
            {
                if (x <= 5000 && x >= 0
                        && y <= 7000 && y >= 0)
                {
                    sPort.Write(addCmd(CMDTYPE_LINE, (uint)((2 * x) / 3), y), 0, ADDCMD_LENGTH);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void SendPen(uint state)
        {
            sPort.Write(addCmd(CMDTYPE_PEN, state, 0), 0, ADDCMD_LENGTH);
        }

        internal void SendRun()
        {
            sPort.Write(run(), 0, RUN_LENGTH);
        }

        internal Byte[] addCmd(Byte cmdType, uint pA, uint pB)
        {
            Byte[] message = new Byte[ADDCMD_LENGTH];
            message[0] = comHeader;
            message[1] = 0x06;
            message[2] = 0x04;
            message[3] = cmdType;
            message[4] = (Byte)((pA >> 0) & 0xff);
            message[5] = (Byte)((pA >> 8) & 0xff);
            message[6] = (Byte)((pB >> 0) & 0xff);
            message[7] = (Byte)((pB >> 8) & 0xff);
            message[8] = comCRC;

            return message;
        }

        internal Byte[] run()
        {
            Byte[] message = new Byte[RUN_LENGTH];
            message[0] = comHeader;
            message[1] = 0x01;
            message[2] = 0x05;
            message[3] = comCRC;

            return message;
        }
    }
}