﻿namespace DoodleBot
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pcbSvg = new System.Windows.Forms.PictureBox();
            this.lblOpenedFile = new System.Windows.Forms.Label();
            this.pcbFilteredSvgDisplay = new System.Windows.Forms.PictureBox();
            this.btnFilterSvg = new System.Windows.Forms.Button();
            this.pcbResult = new System.Windows.Forms.PictureBox();
            this.btnModellResult = new System.Windows.Forms.Button();
            this.btnStartPlot = new System.Windows.Forms.Button();
            this.btnCalib = new System.Windows.Forms.Button();
            this.btnMove = new System.Windows.Forms.Button();
            this.txbX = new System.Windows.Forms.TextBox();
            this.txbY = new System.Windows.Forms.TextBox();
            this.chkbPenDown = new System.Windows.Forms.CheckBox();
            this.btnOpenPort = new System.Windows.Forms.Button();
            this.Port_comboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pcbSvg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFilteredSvgDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbResult)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(12, 12);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 0;
            this.btnOpenFile.Text = "Open file";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(12, 420);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pcbSvg
            // 
            this.pcbSvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbSvg.Location = new System.Drawing.Point(12, 41);
            this.pcbSvg.Name = "pcbSvg";
            this.pcbSvg.Size = new System.Drawing.Size(210, 297);
            this.pcbSvg.TabIndex = 3;
            this.pcbSvg.TabStop = false;
            // 
            // lblOpenedFile
            // 
            this.lblOpenedFile.AutoSize = true;
            this.lblOpenedFile.Location = new System.Drawing.Point(93, 17);
            this.lblOpenedFile.Name = "lblOpenedFile";
            this.lblOpenedFile.Size = new System.Drawing.Size(0, 13);
            this.lblOpenedFile.TabIndex = 4;
            // 
            // pcbFilteredSvgDisplay
            // 
            this.pcbFilteredSvgDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbFilteredSvgDisplay.Location = new System.Drawing.Point(273, 41);
            this.pcbFilteredSvgDisplay.Name = "pcbFilteredSvgDisplay";
            this.pcbFilteredSvgDisplay.Size = new System.Drawing.Size(210, 297);
            this.pcbFilteredSvgDisplay.TabIndex = 3;
            this.pcbFilteredSvgDisplay.TabStop = false;
            // 
            // btnFilterSvg
            // 
            this.btnFilterSvg.Location = new System.Drawing.Point(273, 12);
            this.btnFilterSvg.Name = "btnFilterSvg";
            this.btnFilterSvg.Size = new System.Drawing.Size(75, 23);
            this.btnFilterSvg.TabIndex = 0;
            this.btnFilterSvg.Text = "Filter Svg";
            this.btnFilterSvg.UseVisualStyleBackColor = true;
            this.btnFilterSvg.Click += new System.EventHandler(this.btnFilterSvg_Click);
            // 
            // pcbResult
            // 
            this.pcbResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcbResult.Location = new System.Drawing.Point(534, 41);
            this.pcbResult.Name = "pcbResult";
            this.pcbResult.Size = new System.Drawing.Size(210, 297);
            this.pcbResult.TabIndex = 3;
            this.pcbResult.TabStop = false;
            // 
            // btnModellResult
            // 
            this.btnModellResult.Location = new System.Drawing.Point(534, 12);
            this.btnModellResult.Name = "btnModellResult";
            this.btnModellResult.Size = new System.Drawing.Size(115, 23);
            this.btnModellResult.TabIndex = 0;
            this.btnModellResult.Text = "Prepare for Plotting";
            this.btnModellResult.UseVisualStyleBackColor = true;
            this.btnModellResult.Click += new System.EventHandler(this.btnModellResult_Click);
            // 
            // btnStartPlot
            // 
            this.btnStartPlot.Location = new System.Drawing.Point(840, 391);
            this.btnStartPlot.Name = "btnStartPlot";
            this.btnStartPlot.Size = new System.Drawing.Size(115, 23);
            this.btnStartPlot.TabIndex = 0;
            this.btnStartPlot.Text = "Start Plotting";
            this.btnStartPlot.UseVisualStyleBackColor = true;
            this.btnStartPlot.Click += new System.EventHandler(this.btnStartPlot_Click);
            // 
            // btnCalib
            // 
            this.btnCalib.Location = new System.Drawing.Point(852, 144);
            this.btnCalib.Name = "btnCalib";
            this.btnCalib.Size = new System.Drawing.Size(75, 23);
            this.btnCalib.TabIndex = 6;
            this.btnCalib.Text = "calib";
            this.btnCalib.UseVisualStyleBackColor = true;
            this.btnCalib.Click += new System.EventHandler(this.btnCalib_Click);
            // 
            // btnMove
            // 
            this.btnMove.Location = new System.Drawing.Point(852, 230);
            this.btnMove.Name = "btnMove";
            this.btnMove.Size = new System.Drawing.Size(75, 23);
            this.btnMove.TabIndex = 7;
            this.btnMove.Text = "move";
            this.btnMove.UseVisualStyleBackColor = true;
            this.btnMove.Click += new System.EventHandler(this.btnMove_Click);
            // 
            // txbX
            // 
            this.txbX.Location = new System.Drawing.Point(809, 204);
            this.txbX.Name = "txbX";
            this.txbX.Size = new System.Drawing.Size(71, 20);
            this.txbX.TabIndex = 8;
            // 
            // txbY
            // 
            this.txbY.Location = new System.Drawing.Point(896, 204);
            this.txbY.Name = "txbY";
            this.txbY.Size = new System.Drawing.Size(71, 20);
            this.txbY.TabIndex = 8;
            // 
            // chkbPenDown
            // 
            this.chkbPenDown.AutoSize = true;
            this.chkbPenDown.Location = new System.Drawing.Point(852, 259);
            this.chkbPenDown.Name = "chkbPenDown";
            this.chkbPenDown.Size = new System.Drawing.Size(76, 17);
            this.chkbPenDown.TabIndex = 9;
            this.chkbPenDown.Text = "Pen Down";
            this.chkbPenDown.UseVisualStyleBackColor = true;
            this.chkbPenDown.CheckedChanged += new System.EventHandler(this.chkbPenDown_CheckedChanged);
            // 
            // btnOpenPort
            // 
            this.btnOpenPort.Location = new System.Drawing.Point(913, 52);
            this.btnOpenPort.Name = "btnOpenPort";
            this.btnOpenPort.Size = new System.Drawing.Size(75, 23);
            this.btnOpenPort.TabIndex = 10;
            this.btnOpenPort.Text = "OpenPort";
            this.btnOpenPort.UseVisualStyleBackColor = true;
            this.btnOpenPort.Click += new System.EventHandler(this.btnOpenPort_Click);
            // 
            // Port_comboBox
            // 
            this.Port_comboBox.FormattingEnabled = true;
            this.Port_comboBox.Location = new System.Drawing.Point(809, 25);
            this.Port_comboBox.Name = "Port_comboBox";
            this.Port_comboBox.Size = new System.Drawing.Size(121, 21);
            this.Port_comboBox.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(820, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "X:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(910, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Y:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1000, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Port_comboBox);
            this.Controls.Add(this.btnOpenPort);
            this.Controls.Add(this.chkbPenDown);
            this.Controls.Add(this.txbY);
            this.Controls.Add(this.txbX);
            this.Controls.Add(this.btnMove);
            this.Controls.Add(this.btnCalib);
            this.Controls.Add(this.lblOpenedFile);
            this.Controls.Add(this.pcbResult);
            this.Controls.Add(this.pcbFilteredSvgDisplay);
            this.Controls.Add(this.pcbSvg);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnStartPlot);
            this.Controls.Add(this.btnModellResult);
            this.Controls.Add(this.btnFilterSvg);
            this.Controls.Add(this.btnOpenFile);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pcbSvg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbFilteredSvgDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox pcbSvg;
        private System.Windows.Forms.Label lblOpenedFile;
        private System.Windows.Forms.PictureBox pcbFilteredSvgDisplay;
        private System.Windows.Forms.Button btnFilterSvg;
        private System.Windows.Forms.PictureBox pcbResult;
        private System.Windows.Forms.Button btnModellResult;
        private System.Windows.Forms.Button btnStartPlot;
        private System.Windows.Forms.Button btnCalib;
        private System.Windows.Forms.Button btnMove;
        private System.Windows.Forms.TextBox txbX;
        private System.Windows.Forms.TextBox txbY;
        private System.Windows.Forms.CheckBox chkbPenDown;
        private System.Windows.Forms.Button btnOpenPort;
        private System.Windows.Forms.ComboBox Port_comboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

