﻿using System;
using System.Collections.Generic;
using Svg;

namespace DoodleBot
{
    internal class Path : Shape
    {
        private List<Point> pointList;

        public Path(List<Point> points)
        {
            pointList = new List<Point>();
            pointList = points;
        }

        public Path()
        {
            pointList = new List<Point>();
        }

        public Path(SvgPath elem)
        {
            pointList = new List<Point>();
            int i = 0;
            foreach (var item in (elem).PathData)
            {
                i++;
                if (i != (elem).PathData.Count)
                {
                    this.AddPoint(new Point((uint)item.End.X, (uint)item.End.Y));
                }

            }
        }

        public void AddPoint(Point point)
        {
            pointList.Add(point);
        }

        public List<Point> GetPoints()
        {
            List<Point> returnList = pointList;
            return returnList;
        }

        public override Path ToPath()
        {
            return this;
        }
    }
}