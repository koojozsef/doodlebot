﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Svg;

namespace DoodleBot
{
    class ShapeCreator
    {
        internal static Shape GetShape(SvgElement source)
        {
            Shape returnShape = null;
            switch (determineShapeType(source))
            {
                case ShapeConstants.ShapeTypes.Rectangle:
                    return returnShape = new Rectangle((SvgRectangle)source);
                case ShapeConstants.ShapeTypes.Path:
                    return returnShape = new Path((SvgPath)source);
                case ShapeConstants.ShapeTypes.NoShape:
                default:
                    break;
            }
            
            return returnShape;
        }

        private static ShapeConstants.ShapeTypes determineShapeType(SvgElement source)
        {
            if (source.GetType() == ShapeConstants.SvgRectangleType.GetType())
            {
                return ShapeConstants.ShapeTypes.Rectangle;
            }
            if (source.GetType() == ShapeConstants.SvgPointType.GetType())
            {
                return ShapeConstants.ShapeTypes.Point;
            }
            if (source.GetType() == ShapeConstants.SvgPathType.GetType())
            {
                return ShapeConstants.ShapeTypes.Path;
            }
            return ShapeConstants.ShapeTypes.NoShape;
        }
    }
}
