﻿using System;
using Svg;

namespace DoodleBot
{
    internal class Rectangle : Shape
    {
        public uint x { get; private set; }
        public uint width { get; private set; }
        public uint y { get; private set; }
        public uint height { get; private set; }

        public Rectangle(SvgRectangle elem)
        {
            x = (uint)elem.X;
            y = (uint)elem.Y;
            width = (uint)elem.Width;
            height = (uint)elem.Height;
        }

        public override Path ToPath()
        {
            Path pathItem = new Path();
            //first corner
            pathItem.AddPoint(new Point(x, y));

            //second corner
            pathItem.AddPoint(new Point(x, y + height));

            //third corner
            pathItem.AddPoint(new Point(x + width, y + height));

            //fourth corner
            pathItem.AddPoint(new Point(x + width, y));

            //first corner for finish
            pathItem.AddPoint(new Point(x, y));

            return pathItem;
        }
    }
}