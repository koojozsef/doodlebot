﻿using System;

namespace DoodleBot
{
    internal class Point : Shape
    {
        public uint X { get; set; }
        public uint Y { get; set; }

        public Point(uint x, uint y)
        {
            X = x;
            Y = y;
        }

        public override Path ToPath()
        {
            Path path = new Path();
            path.AddPoint(this);
            return path;
        }
    }
}