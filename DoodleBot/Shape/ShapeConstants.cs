﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Svg;

namespace DoodleBot
{
    static class ShapeConstants
    {
        public enum ShapeTypes
        {
            NoShape,
            Rectangle,
            Path,
            Point
        }

        public static SvgRectangle SvgRectangleType = new SvgRectangle();
        public static SvgPath SvgPathType = new SvgPath();
        public static SvgPoint SvgPointType = new SvgPoint();
    }
}
