﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoodleBot
{
    public class ControllerClass
    {
        //connected classes:
        SVGProcessor svgProcessor;
        ShapeCreator shapeCreator;
        ConverterClass converterClass;
        Communication communication;

        //storage
        public string CurrentFileName { get; internal set; }
        public string FilteredFileName { get; private set; }

        private List<Shape> shapesList;
        private List<Shape> pathList;

        //constants
        private const uint SIZE_RATE = 10;
        public const uint DISPLAY_WIDTH = 210;
        public const uint DISPLAY_HEGHT = 297;

        //singleton pattern
        private static ControllerClass instance;

        internal static ControllerClass GetInstance()
        {
            try
            {
                if (instance == null)
                {
                    instance = new ControllerClass();
                }
                return instance;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private ControllerClass()
        {
            try
            {
                svgProcessor = new SVGProcessor();
                shapeCreator = new ShapeCreator();
                converterClass = new ConverterClass();
                shapesList = new List<Shape>();
                pathList = new List<Shape>();
                communication = new Communication();
            }
            catch (Exception)
            {

                throw;
            }
        }
        //singleton pattern -end
        

        internal Image drawOriginalFie()
        {
            try
            {
                return svgProcessor.FileToBitmap(svgProcessor.OriginalFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void LoadFile()
        {
            try
            {
                svgProcessor.LoadOriginalFile(CurrentFileName);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void Exit()
        {
            communication.ClosePort();
            //close everithing which need to be closed
        }

        internal void ReInit()
        {
            shapesList = new List<Shape>();
            pathList = new List<Shape>();
            svgProcessor.ReInit();
        }

        internal void FilterCurrentSvgFile()
        {
            try
            {
                svgProcessor.Filter(CurrentFileName);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal Image drawFilteredFile()
        {
            try
            {
                return svgProcessor.FileToBitmap(svgProcessor.FilteredFile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Convert the FilteredFile to an acceptable form
        ///     - generates the needed classes
        ///     - create the objects for the picture
        ///     - create a list of objects
        ///     - create a list of path
        /// </summary>
        internal void Understand()
        {
            try
            {
                svgProcessor.ProcessSvg(svgProcessor.FilteredFile);
                //while (svgProcessor.IsThereMoreShapes)
                //{
                //    shapesList.Add(shapeCreator.GetShape(svgProcessor.GetNextShape()));
                //}
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void Print()
        {
            try
            {
                //communication.send();
                List<Path> paths = svgProcessor.getList();
                bool IsFirstPointOfPath = true;
                foreach (Path path in paths)
                {
                    IsFirstPointOfPath = true;
                    foreach (Point point in path.GetPoints())
                    {
                        if (IsFirstPointOfPath == true)
                        {
                            communication.pen(Communication.PEN_UP);
                            communication.SendMove((uint)point.X * SIZE_RATE, (uint)point.Y * SIZE_RATE);
                            communication.pen(Communication.PEN_DOWN);
                            IsFirstPointOfPath = false;
                        }
                        else
                        {
                            communication.SendLine((uint)point.X * SIZE_RATE, (uint)point.Y * SIZE_RATE); 
                        }
                        
                    }
                }
                communication.pen(Communication.PEN_UP);
                communication.SendRun();
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void comRun()
        {
            communication.SendRun();
        }

        internal Image drawResultPicture()
        {
            try
            {

                return svgProcessor.GetFilteredImage();
                //pathList = converterClass.ShapesToPathes(shapesList);
                //return converterClass.PathesToImage(pathList);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void comCalib()
        {
            try
            {
                communication.SendCalib();
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        internal void comMove(uint x, uint y)
        {
            try
            {
                communication.SendLine(x, y);
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal void comPen(uint state)
        {
            communication.SendPen(state);
        }

        internal void OpenPort(string text)
        {
            communication.OpenPort(text);
        }
    }
}
