﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoodleBot
{
    public partial class Form1 : Form
    {
        private ControllerClass controller;
        private OpenFileDialog fileChooser;
        public Form1()
        {
            try
            {
                InitializeComponent();
                controller = ControllerClass.GetInstance();
                fileChooser = new OpenFileDialog();
                InitializeFilechooser(fileChooser);
                btnFilterSvg.Enabled = false;
                btnModellResult.Enabled = false;
                btnStartPlot.Enabled = true;

                string[] ports = SerialPort.GetPortNames();
                Port_comboBox.DataSource = ports;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void InitializeFilechooser(OpenFileDialog fileChooser)
        {
            try
            {
                fileChooser.Multiselect = false;
                fileChooser.InitialDirectory = Directory.GetCurrentDirectory();
                fileChooser.Filter = "Vector graphic files |*.svg";
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            try
            {
                btnFilterSvg.Enabled = false;
                btnModellResult.Enabled = false;
                btnStartPlot.Enabled = true;

                fileChooser.ShowDialog(this);
                if (fileChooser.FileName != "")
                {
                    controller.ReInit();
                    controller.CurrentFileName = fileChooser.FileName;
                    controller.LoadFile();
                    pcbSvg.Image = controller.drawOriginalFie();
                    lblOpenedFile.Text = "Opened file is: " + fileChooser.SafeFileName;

                    btnFilterSvg.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                controller.Exit();
                Application.Exit();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnFilterSvg_Click(object sender, EventArgs e)
        {
            try
            {
                btnModellResult.Enabled = false;
                btnStartPlot.Enabled = true;

                controller.FilterCurrentSvgFile();
                pcbFilteredSvgDisplay.Image = controller.drawFilteredFile();

                btnModellResult.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnModellResult_Click(object sender, EventArgs e)
        {
            try
            {
                btnStartPlot.Enabled = true;

                controller.Understand();
                pcbResult.Image = controller.drawResultPicture();

                btnStartPlot.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnStartPlot_Click(object sender, EventArgs e)
        {
            try
            {
                controller.comCalib();
                controller.Print();
                //controller.comRun();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void ShowErrorMessage()
        {
            MessageBox.Show("error");
        }
        private void ShowErrorMessage(Exception ex)
        {
            MessageBox.Show(ex.TargetSite.Name + "()\n" + ex.Message);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                controller.Print();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnCalib_Click(object sender, EventArgs e)
        {
            try
            {
                controller.comCalib();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            try
            {
                controller.comMove(uint.Parse(txbX.Text), uint.Parse(txbY.Text));
            }
            catch (Exception ex)
            {

                ShowErrorMessage(ex);
            }
        }

        private void chkbPenDown_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                uint state = chkbPenDown.Checked ? (uint)1 : (uint)0;
                controller.comPen(state);
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }

        private void btnOpenPort_Click(object sender, EventArgs e)
        {
            try
            {
                controller.OpenPort(Port_comboBox.SelectedItem.ToString());
                btnOpenPort.Enabled = false;
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex);
            }
        }
    }
}
